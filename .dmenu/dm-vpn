#!/bin/bash
##____  _                      _
#|  _ \| |__   ___   ___ _ __ (_)_  __
#| |_) | '_ \ / _ \ / _ \ '_ \| \ \/ /
#|  __/| | | | (_) |  __/ | | | |>  <
#|_|   |_| |_|\___/ \___|_| |_|_/_/\_\
# Name: dm-vpn
# Description: Dm script to connect to a vpn
# Author: Karl Fredin

# Script to connect to a vpn provider

# Source all the configuration
source $HOME/.config/dmscripts/config.sh

NOTIFY(){
	msg="client for mullvad is not installed"
	notify-send "$msg"
	error "$msg"
}

MULLVAD_VPN() {
   # The main option
   connect=$(echo "Disconnect" "Connect" "Random" "Country" | xargs -n1)

   # List all of the countries you can connect to
   countries=$(mullvad relay list | awk '{print $1}' | grep -v -e "^[A-Z]" -e "^$")

   # Gets a random country to connect to
   random_list=$(mullvad relay list | awk '{print $1}' | grep -v -e "^[A-Z]" -e "^$" | sort -R | head -n1)

   # Declare the countries array
   declare -a options=("$countries")

   # functions to use to connect to the server
   function vpn_connect() {
        mullvad connect
   }

   function vpn_disconnect() {
        mullvad disconnect
   }

   function vpn_random() {
        mullvad relay set hostname "$random_list"
   }

   function vpn_country() {
        mullvad relay set hostname $choice
   }
}

NORD_VPN() {
   # The main option
   connect=$(echo "Disconnect" "Connect" "Random" "Country" | xargs -n1)

   # List all of the countries you can connect to
   countries=$(nordvpn countries 2> /dev/null | xargs -n1 | sed -e '/^-/d' -e 's/,//')

   # Gets a random country to connect to
   random_list=$(nordvpn countries 2> /dev/null | xargs -n1 | sed -e '/^-/d' -e 's/,//' | sort -R | head -n1)

   # Declare the countries array
   declare -a options=("$countries")

   # functions to use to connect to the server
   function vpn_connect() {
        nordvpn connect
   }

   function vpn_disconnect() {
        nordvpn disconnect
   }

   function vpn_random() {
        nordvpn connect "$random_list"
   }

   function vpn_country() {
        nordvpn connect "$choice"
   }
}

# Vpn of choice
if [ "$VPN" = "mullvad" ] ; then
	command_exist "mullvad" || NOTIFY
	MULLVAD_VPN

elif [ "$VPN" = "nordvpn" ] ; then
	command_exist "nordvpn" || NOTIFY
	NORD_VPN
fi


# Make your first choice
choice=$(printf '%s\n' "${connect[@]}" | MENU  "Vpn" )

# Exit if you dont make a choice
[ -z $choice ] && exit

case "$choice" in 
	"Disconnect") vpn_disconnect;;
	"Connect") vpn_connect;;
	"Random") vpn_random;;
	"Country")
		choice=$(printf '%s\n' "${options[@]}" |  MENU "Choose Country:" ) && vpn_country
		;;
esac



