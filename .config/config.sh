#!/bin/bash
##____  _                      _      
#|  _ \| |__   ___   ___ _ __ (_)_  __
#| |_) | '_ \ / _ \ / _ \ '_ \| \ \/ /
#|  __/| | | | (_) |  __/ | | | |>  < 
#|_|   |_| |_|\___/ \___|_| |_|_/_/\_\
#                                     
# -*- coding: utf-8 -*-
#
# Configuration file for my dm script
# Here all my configuration for my run menu script will be stored
# So I can just import all settings and use it accross all my various
# Dmenu scripts

# Import shared configurations
# for example adding support for flags
# and adding help function

DMS_PATH="$HOME/myrepos/dmscripts"
HELPER_="$DMS_PATH/.config/_helper.sh"
source "$HELPER_"

# Set rofi theme and launcher here
ROFIDIR="$HOME/.config/rofi/launchers/type-1"
ROFITHEME='style-16'

# Path to my dmenu scripts and cache folder
CACHE_DIR="$HOME/.cache/dmscripts"
DMENU_PATH=$HOME/.local/share/dmenu
DMENU_SCRIPT=$(ls "$DMENU_PATH")

# Path to my chezmoi/dotfiles directory
DOTFILES_REPO="$HOME/.local/share/chezmoi"

# Choose vpn
# Valid nordvpn and mullvad
VPN=mullvad

# Choose your preferred browser
BROWSER=zen-browser

# Only chromium based browsers are supported here
# since firefox based browser does not support application mode
FULLSCREEN_BROWSER=microsoft-edge-stable

# File containing my bookmarks/quickmarks
URL="$HOME/.config/qutebrowser/quickmarks"

# PDF viewer of choice
PDF_VIEWER=zathura

# SSH User
USER_SSH=karl

# Terminal of choice
TERM=kitty

# Text editors of choice
# GUI_EDITOR="emacsclient -c -a emacs"
# GUI_EDITOR="$TERM -e nvim"
GUI_EDITOR="neovide"

# Terminal editor used with -f or -t flag
TUI_EDITOR="nvim"

# The amount of histtory items to show in dm-clip
CLIP_HISTORY=200

# Setting image viewer to sxiv
IMAGE_VIEWER="sxiv"



