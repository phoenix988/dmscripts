##____  _                      _      
#|  _ \| |__   ___   ___ _ __ (_)_  __
#| |_) | '_ \ / _ \ / _ \ '_ \| \ \/ /
#|  __/| | | | (_) |  __/ | | | |>  < 
#|_|   |_| |_|\___/ \___|_| |_|_/_/\_\
#                                     
# -*- coding: utf-8 -*-
#

# shared functions used for my dmscripts

fmt_help="  %-20s\t%-54s\n"
USED_SCRIPT=$(echo $0 | awk -F "/" '{print $NF}')
function HELP_INFO() {
    echo "Description: Options available for my dmscripts."
    echo ""
    echo "Usage: [dm-name] [OPTION]"
    printf "${fmt_help}" \
        "-h, --help, help" "Print this help." \
        "-f, --fzf, fzf" "use fzf in the terminal." \
        "-r, --rofi, rofi" "use rofi as your run launcher." \
        "-w, --wofi, wofi" "use wofi as your run launcher."\

     # switches that only works on certain scripts
    case "$USED_SCRIPT" in
      dm-edit)
        printf "${fmt_help}" \
          "-g, --generate, generate" "generate cache file to use." \
          "(will create one if it does not exist)"
        ;;
      dm-bookmarks)
        printf "${fmt_help}" \
          "-F, --fullscreen, fullscreen" "open the URL in fullscreen (application) mode."  \
          "-b, --browser, browser" "select another browser Available (qutebrowser | firefox | edge)." 
        ;;
    esac

    exit 0
}

# Error function
function error () {
  clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

# checks if a command exist or not
function command_exist() {
  type "$1" &> /dev/null;
}

while getopts ":rwWfgthFb:-:" opt; do
  case ${opt} in
    b) # change browser from the default
      case ${OPTARG} in
        "qutebrowser" )
          [ "$USED_SCRIPT" == "dm-bookmarks" ] || error "Invalid option: -b"
          BROWSER="qutebrowser"
          ;;
        "edge" )
          [ "$USED_SCRIPT" == "dm-bookmarks" ] || error "Invalid option: -b"
          BROWSER="microsoft-edge-stable"
          ;;
        "firefox" )
          [ "$USED_SCRIPT" == "dm-bookmarks" ] || error "Invalid option: -b"
          BROWSER="firefox"
          ;;
        *)
          [ "$USED_SCRIPT" == "dm-bookmarks" ] || error "Invalid option: -b"
          echo "${RED}:Invalid value. Please select qutebrowser, firefox, chromium or edge"
          exit 1
          ;;
        :)
          [ "$USED_SCRIPT" == "dm-bookmarks" ] || error "Invalid option: -b"
          echo "Option -$OPTARG requires an argument." >&2
          exit
      ;;
      esac
      ;;
    r)
      # use rofi
      menu="rofi"
      ;;
    w)
      # use wofi
      menu="wofi"
      ;;
    W)
      # use wofi
      menu="wofi"
      ;;
    f)
      # use fzf only works in terminal
      menu="fzf"
      GUI_EDITOR=$TUI_EDITOR
      ;;
    g) 
      [ "$USED_SCRIPT" == "dm-edit" ] || error "Invalid option: -g"
      GENERATE=true
      ;; # Generate cache file for dm-editconfig
    F) 
      [ "$USED_SCRIPT" == "dm-bookmarks" ] || error "Invalid option: -F"
      FULLSCREEN=true;; # Choose if you want to open your bookmark in fullscreen in dm-bookmark
    t) FZF='y'
       menu="fzf";;
    h) HELP_INFO ;;
    -)
      case "${OPTARG}" in
        rofi)
          menu="rofi"
          ;;
        fzf)
          menu="fzf"
          GUI_EDITOR=$TUI_EDITOR
          ;;
        wofi)
          menu="wofi"
          ;;
        generate)
          [ "$USED_SCRIPT" == "dm-edit" ] || error "Invalid option: --generate"
          GENERATE=true
          ;;
        fullscreen) 
          [ "$USED_SCRIPT" == "dm-bookmarks" ] || error "Invalid option: --fullscreen"
          FULLSCREEN=true;; # Choose if you want to open your bookmark in fullscreen in dm-bookmark
        help)
          HELP_INFO
          ;;
        *)
          echo "Invalid option: --${OPTARG}" >&2
          exit 1
          ;;
      esac
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit
      ;;
    :)
      [ "$USED_SCRIPT" == "dm-bookmarks" ] || error "Invalid option: -b"
      echo "Option -$OPTARG requires an argument." >&2
      exit
      ;;
  esac
done

shift $((OPTIND -1))

# Handle positional arguments
for arg in "$@"; do
  case "$arg" in
    rofi)
      menu="rofi"
      ;;
    fzf)
      menu="fzf"
      GUI_EDITOR=$TUI_EDITOR
      ;;
    wofi)
      menu="wofi"
      ;;
    help)
      HELP_INFO
      ;;
    generate)
      [ "$USED_SCRIPT" == "dm-edit" ] || error "Invalid option: generate"
      GENERATE=true
      ;;
    fullscreen) 
      [ "$USED_SCRIPT" == "dm-bookmarks" ] || error "Invalid option: --fullscreen"
      FULLSCREEN=true;; # Choose if you want to open your bookmark in fullscreen in dm-bookmark
    *)
      echo "Invalid option: $arg"
      exit 1
      ;;
  esac
done

# setting the run launcher
# to rofi, fzf or fuzzel
function MENU(){ 
  if [ "$menu" = "rofi" ] ; then
    rofi \
      -dmenu \
      -theme ${ROFIDIR}/${ROFITHEME}.rasi -p "$1" -mesg "$1"
  elif [ "$menu" = "fzf" ] ; then
    fzf 
  elif [ "$menu" = "wofi" ] ; then
    # fuzzel --dmenu -p "$1" 
    wofi -dmenu
    # check if you use wayland or xorg
    # only applies if you did not specify a flag
    # using fuxxel for way and rofi for xorg
  elif [ "$XDG_SESSION_TYPE" = "wayland" ] ; then

    # fuzzel --dmenu -p "$1" 
    rofi \
      -dmenu \
      -theme ${ROFIDIR}/${ROFITHEME}.rasi -p "$1" -mesg "$1"

  else

    # Use rofi if you are on xorg
    rofi \
      -dmenu \
      -theme ${ROFIDIR}/${ROFITHEME}.rasi -p "$1" -mesg "$1"
  fi
}

# normal application launcher for rofi
function run_launcher() {
  rofi \
    -show drun \
    -theme ${ROFIDIR}/${ROFITHEME}.rasi -p "$1"
}

function RUN_HUB(){
  # runs the choosen script
  if [ "$menu" = "rofi" ] ; then
    "$DMENU_PATH/dm-hub" -r

  elif [ "$menu" = "fzf" ] ; then
    "$DMENU_PATH/dm-hub" -f
  elif [ "$menu" = "wofi" ] ; then
    "$DMENU_PATH/dm-hub" -w
    # check if you use wayland or xorg
  elif [ "$XDG_SESSION_TYPE" = "wayland" ] ; then
    "$DMENU_PATH/dm-hub" -w

  else
    "$DMENU_PATH/dm-hub" -r
    fi

  }
